import Amplify from 'aws-amplify';
import awsconfig from './aws-exports';
import { AmplifyAuthenticator, AmplifySignIn, AmplifySignUp } from '@aws-amplify/ui-react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';
import Notes from './pages/Notes/Notes';
import { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from './app/hooks';
import { loadUser, selectUserState, setUser } from './redux/user-slice/user-slice';
import { Alert, Spinner } from 'reactstrap';
import UserService from './services/user-service';
import dotenv from 'dotenv';

Amplify.configure(awsconfig);

const configResult = dotenv.config()

if (configResult.error) {
  throw configResult.error
}

toast.configure();


const App = () => {

  const [isLoadingUser, setIsLoadingUser] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const userState = useAppSelector(selectUserState);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (userState.status === "idle" && !userState.user) {
      dispatch(loadUser());
    }
  }, [userState, dispatch]);



  const handleAuthStateChange = async (state: any) => {
    if (state === 'signedin') {
      await setCurrentUser()
    }
  }

  const setCurrentUser = async () => {
    try {
      setIsLoadingUser(true);

      const user = await UserService.getUser();

      dispatch(setUser(user!));
      setIsLoadingUser(false);
    } catch (error: any) {
      setErrorMessage(error);
    }

  }

  if (userState.status === "loading" || isLoadingUser) {
    return <div className="loading"><Spinner /></div> 
  }

  if (errorMessage || userState.error) {
    return <Alert color="danger">{errorMessage ?? userState.error}. Try refeshing the page</Alert>
  }



  return (
    <AmplifyAuthenticator usernameAlias="email">
      <AmplifySignUp
        slot="sign-up"
        usernameAlias="email"
        formFields={[
          {
            type: "email",
            inputProps: { required: true, autocomplete: "username" },
          },
          {
            type: "password",
            inputProps: { required: true, autocomplete: "new-password" },
          },
          {
            type: "given_name",
            label: "First Name *",
            placeholder: "First Name",
            inputProps: { required: true, autocomplete: "first-name" },
          },
          {
            type: "family_name",
            label: "Last Name *",
            placeholder: "Last Name",
            inputProps: { required: true, autocomplete: "family-name" },
          }
        ]}
      />
      {
        userState.user &&
        <Notes />
      }
      <AmplifySignIn handleAuthStateChange={handleAuthStateChange} slot="sign-in" usernameAlias="email" />

    </AmplifyAuthenticator>
  );
};


export default App;
