import { act, render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import User from "../../interfaces/user";
import { toast } from 'react-toastify';
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import AddNoteButton from "./AddNoteButton";
import { configureStore } from "@reduxjs/toolkit";
import UserReducer from '../../redux/user-slice/user-slice';
import NotesReducer, { NotesState } from '../../redux/note-slice/note-slice';
import userEvent from '@testing-library/user-event'

const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
}

const color = '#f44336';
const content = 'Test content';
const title = 'Test Title';
const mockedDispatch = jest.fn();


jest.mock('react-toastify');

const preloadedStore = (notesState: NotesState) => {
    const store = configureStore({
        reducer: { user: UserReducer, notes: NotesReducer }, preloadedState: {
            notes: notesState,
            user: {
                error: undefined,
                status: 'success',
                user: user
            }
        }
    });

    store.dispatch = mockedDispatch;
    return store;
}

const renderAddButton = (failedState = false) => {
    const notesState: NotesState = {
        error: undefined,
        notes: undefined,
        searchText: '',
        status: failedState ? 'failed' : 'success',
        hasMore: true
    };

    const store = preloadedStore(notesState);

    render(
        <Provider store={store}>
            <AddNoteButton />
        </Provider>
    );

    return store;
}


const openModal = async () => {
    const addNoteButtons = await screen.findAllByText('Add note');
    const addNoteButton = addNoteButtons[0];

    act(() => {
        userEvent.click(addNoteButton);
    });
}

const selectNoteColor = async () => {
    const noteColor = await screen.findByTitle(color);

    act(() => {
        userEvent.click(noteColor);
    });
}

const setNoteTitle = async () => {
    const noteTitle = await screen.findByPlaceholderText('Title');
    act(() => {
        userEvent.type(noteTitle, title); 
    })
    
}

const setNoteContent = async () => {
    const noteContent = await screen.findByPlaceholderText('Content');
    act(() => {
        userEvent.type(noteContent, content); 
    })
    
}

const clickSaveButton = () => {
    const saveButton = screen.getByText('Save');

    act(() => {
        userEvent.click(saveButton);
    })
}

describe('Add Note Button', () => {

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Clicking add note button opens modal', async () => {
        renderAddButton();
        await openModal();


        await expect(screen.findByText('New Note')).resolves.toBeInTheDocument();
    });

    it('Clicking cancel closes modal', async () => {
        renderAddButton();
        await openModal();

        const cancelButton = screen.getByText('Cancel');

        act(() => {
            userEvent.click(cancelButton);
        })

        expect(cancelButton).not.toBeInTheDocument();
    });

    it('Error message is shown if note is saved without color', async () => {
        renderAddButton();
        await openModal();
        await setNoteContent();
        await setNoteTitle();

        clickSaveButton();

        expect(toast.warning).toBeCalledWith('Please select a note color');
    });

    it('Save note actions is not dispatched if note is saved without color', async () => {
        renderAddButton();
        await openModal();
        await setNoteContent();
        await setNoteTitle();

        clickSaveButton();

        expect(mockedDispatch).toBeCalledTimes(0);
    });

    it('Error message is shown if note is saved without title', async () => {
        renderAddButton();
        await openModal();
        await selectNoteColor();
        await setNoteContent();

        clickSaveButton();

        expect(toast.warning).toBeCalledWith('Note must have a title');
    });

    it('Save note actions is not dispatched if note is saved without title', async () => {
        renderAddButton();
        await openModal();
        await selectNoteColor();
        await setNoteContent();

        clickSaveButton();

        expect(mockedDispatch).toBeCalledTimes(0);
    });

    it('Error message is shown if note is saved without content', async () => {
        renderAddButton();
        await openModal();
        await selectNoteColor();
        await setNoteTitle();

        clickSaveButton();

        expect(toast.warning).toBeCalledWith('Note must have content');
    });

    it('Save note actions is not dispatched if note is saved without content', async () => {
        const store = renderAddButton();
        await openModal();
        await selectNoteColor();
        await setNoteTitle();

        clickSaveButton();

        expect(mockedDispatch).toBeCalledTimes(0);
    });

    it('Actions is dispatched when the save button is clicked and add note form is filled', async () => {
        renderAddButton();
        mockedDispatch.mockReturnValue({meta: {requestStatus: 'fulfilled'}});
        await openModal();
        
        const modalTitle = await screen.findByText('New Note');

        await selectNoteColor();
        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitForElementToBeRemoved(modalTitle);

        expect(mockedDispatch).toBeCalledTimes(1);
        
    });

    it('Modal is closed when note is saved successfully', async () => {
        renderAddButton();
        mockedDispatch.mockReturnValue({meta: {requestStatus: 'fulfilled'}});
        await openModal();
        
        const modalTitle = await screen.findByText('New Note');

        await selectNoteColor();
        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitForElementToBeRemoved(modalTitle);
    });

    it('Modal is remains open when there is an error saving a note', async () => {
        renderAddButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();
        
        const modalTitle = await screen.findByText('New Note');

        await selectNoteColor();
        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitFor(mockedDispatch);
        expect(modalTitle).toBeInTheDocument();
    });

    it('Toast is shown when there is an error saving a note', async () => {
        renderAddButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();

        await selectNoteColor();
        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitFor(mockedDispatch);
        expect(toast.error).toBeCalledTimes(1);
        expect(toast.error).toBeCalledWith('Error saving note, try again');
    });

    it('Form fields are disabled while saving', async () => {
        renderAddButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();

        await selectNoteColor();
        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();    
        
        const titleInput = screen.getByPlaceholderText('Title');
        const contentInput = screen.getByPlaceholderText('Content');

        expect(titleInput).toBeDisabled();
        expect(contentInput).toBeDisabled();
        
        await waitFor(mockedDispatch);
    });

    it('Modal is not open if notes state is failed', async () => {
        renderAddButton(true);
        await openModal();     
        await expect(screen.findByText('New Note')).rejects.toThrow();
    });

});
