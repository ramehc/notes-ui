import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { toast } from "react-toastify";
import { Button, Form, FormGroup, Modal, ModalBody, Spinner } from "reactstrap";
import { useAppDispatch } from "../../app/hooks";
import { Note } from "../../interfaces/note";
import { deleteNote } from "../../redux/note-slice/note-slice";
import './DeleteNote.css';

const DeleteNoteButton: React.FunctionComponent<{ note: Note }> = (props) => {

    const dispatch = useAppDispatch();

    const [modal, setModal] = useState(false);
    const [isDeleting, setIsDeleting] = useState(false);
    const toggle = () => setModal(!modal);


    const onDeleteNote = async () => {
        setIsDeleting(true);
        setModal(false);

        try {
            const response = await dispatch(deleteNote(props.note.id));
            if (response.meta.requestStatus === 'rejected') {
                throw new Error();
            }
        } catch (error) {
            setIsDeleting(false);
            setModal(true);
            toast.error(`Error deleting '${props.note.title}', try again`, {
                type: "error"
            })
        }
    };

    return <>
        {
            isDeleting ? <Spinner className="delete__note"/> :
            <FontAwesomeIcon data-cy="delete-note" onClick={toggle} title='Delete note' className="delete__note" icon={faTrashAlt} />
        }


        {
            modal &&

            <Modal isOpen={true} toggle={toggle} >
                <ModalBody>
                    <Form>
                        <legend>Delete Note</legend>
                        <FormGroup>
                            {`Are you sure you want to delete '${props.note.title}'? This cannot be undone `}
                        </FormGroup>
                        <Button data-cy='confirm-delete' disabled={isDeleting} color="danger" onClick={onDeleteNote}>{
                            isDeleting ? <Spinner title="Deleting note" size={"sm"} /> : "Delete"
                        }</Button> {' '}
                        <Button disabled={isDeleting} color="secondary" onClick={toggle}>Cancel</Button>
                    </Form>
                </ModalBody>
            </Modal>
        }
    </>
}

export default DeleteNoteButton;