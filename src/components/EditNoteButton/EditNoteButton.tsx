import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { CirclePicker } from "react-color";
import { toast } from "react-toastify";
import { Button, Form, FormGroup, Input, Label, Modal, ModalBody, Spinner } from "reactstrap";
import { useAppDispatch } from "../../app/hooks";
import { Note } from "../../interfaces/note";
import { editNote } from "../../redux/note-slice/note-slice";
import './EditNoteButton.css';

const EditNoteButton: React.FunctionComponent<Note> = (prop) => {
    
    const dispatch = useAppDispatch();
    const [isSaving, setIsSavingNote] = useState(false);
    const [isEditing, setIsEditingNote] = useState(false);
    const [title, setNoteTitle] = useState<string>(prop.title);
    const [content, setNoteContent] = useState<string>(prop.content);
    const [color, setNoteColor] = useState<string>(prop.color);

    const onSaveNote: React.MouseEventHandler<HTMLButtonElement> = async (event: React.MouseEvent) => {
        event.stopPropagation();

        if (validateField(title)) {
            toast.warning('Note must have a title');
            return;
        }

        if (validateField(content)) {
            toast.warning('Note must have content');
            return;
        }

        setIsSavingNote(true);

        try {
            const response = await dispatch(editNote({
                color: color,
                content: content,
                title: title,
                id: prop.id
            }));

            if (response.meta.requestStatus === "rejected") {
                throw new Error();
            }

            setIsEditingNote(false);
        } catch (error) {
            toast.error('Error editing note, try again');
        }

        setIsSavingNote(false);
    }

    const validateField = (value: string | undefined) => !value || value.trim() === '';

    const toggle = () => {

        if(isEditing){
            setNoteTitle(prop.title);
            setNoteContent(prop.content);
            setNoteColor(prop.color);
        }

        setIsEditingNote(!isEditing)
    };

    return <>

        <FontAwesomeIcon data-cy="edit-note" onClick={toggle} title={'Edit note'} className="edit__note d-none d-sm-inline-block" icon={faPencilAlt} size={'2x'} />
        <FontAwesomeIcon data-cy="edit-note" onClick={toggle} title={'Edit note'} className="edit__note d-inline-block d-sm-none" icon={faPencilAlt} size={'1x'} />

        {
            isEditing &&

            <Modal isOpen={isEditing} toggle={toggle} >
                <ModalBody>
                    <Form>
                        <legend>Edit Note</legend>
                        <FormGroup>
                            <div style={{display: 'inline-block'}} >Color*: &nbsp; <div style={{display: 'inline-block', backgroundColor: `${color ?? prop.color}`, border: "1px solid black", height: "15px", width: "15px"}}></div></div>
                            <CirclePicker onChange={(selectedColor) => setNoteColor(selectedColor.hex)}  styles={{ default: { card: { marginTop: '10px', marginBottom: '10px', paddingTop: '10px', paddingLeft: '10px' , backgroundColor: 'whitesmoke' } } }} circleSize={15} width={"100%"} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="new__note__title">Title*</Label>
                            <Input disabled={isSaving} data-cy="note-title" value={title} type="text" name="title" id="new__note__title" placeholder="Title" onChange={(event) => setNoteTitle(event.target.value)} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="new__note__content">Content*</Label>
                            <Input disabled={isSaving} data-cy="note-content" value={content} type="textarea" name="text" id="new__note__content" placeholder="Content" onChange={(event) => setNoteContent(event.target.value)} />
                        </FormGroup>
                        <Button disabled={isSaving} data-cy='save-note' color="primary" onClick={onSaveNote}>{
                            isSaving ? <Spinner title="Saving note" size={"sm"} /> : "Save Changes"
                        }</Button> {' '}
                        <Button disabled={isSaving} color="secondary" onClick={toggle}>Cancel</Button>
                    </Form>
                </ModalBody>
            </Modal>
        }
    </>
}

export default EditNoteButton;