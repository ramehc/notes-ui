import { act, fireEvent, render, screen } from "@testing-library/react";
import MenuBar from "./MenuBar";
import User from "../../interfaces/user";
import { Auth } from 'aws-amplify';
import '@testing-library/jest-dom';
import { store } from "../../app/store";
import { Provider } from "react-redux";

const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
}


jest.mock('aws-amplify');

describe('Menu Bar', () => {

    beforeEach(() => {
        jest.clearAllMocks();
        render(
            <Provider store={store}>
                <MenuBar user={user}></MenuBar>
            </Provider>
        
        );
    });
    it('User name is displayed', () => {
        expect(screen.getByText(`${user.firstName} ${user.lastName}`)).toBeInTheDocument();
    });

    it('Clicking signout icon triggers signout function', () => {
        clickSignOut();
        expect(Auth.signOut).toBeCalledTimes(1);
    });

});

const clickSignOut = () => {
    const signoutIcons = screen.getAllByTitle('Sign out');

    if (signoutIcons.length === 0) {
        fail('Cannot find signout icon');
    }

    const signoutIcon = signoutIcons[0];

    act(() => {
        fireEvent.click(signoutIcon);
    });
}
