import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Avatar from 'react-avatar';
import { Col, Row, Spinner } from 'reactstrap';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import User from '../../interfaces/user';
import { selectUserState, signOutUser } from '../../redux/user-slice/user-slice';
import './MenuBar.css';
import AddNoteButton from '../AddNoteButton/AddNoteButton';

const MenuBar: React.FunctionComponent<{ user: User }> = (props) => {

    const username = `${props.user.firstName} ${props.user.lastName}`;
    const dispatch = useAppDispatch();
    const userState = useAppSelector(selectUserState);
    

    const isLoading = userState.status === "loading";

    return <>
        <Row id="menu__bar">
            <Col xs="7" id="user__details">
                <Avatar size='31' round='15px' className="user__avatar d-none d-sm-inline-block" name={username} ></Avatar>
                <Avatar size='20' round='13px' className="user__avatar d-inline-block d-sm-none" name={username} ></Avatar>
                &nbsp;
                <span id="user__name">{`${props.user.firstName} ${props.user.lastName}`}</span>

            </Col>
            <Col id="menu__options" xs="4">
                <AddNoteButton/>
                &nbsp;
                &nbsp;
                {
                    isLoading? <Spinner></Spinner> :
                    <>
                        <FontAwesomeIcon onClick={() => dispatch(signOutUser())} title="Sign out" className="sign__out d-none d-sm-inline-block" icon={faSignOutAlt} size={'2x'} />
                        <FontAwesomeIcon onClick={() => dispatch(signOutUser())} title="Sign out" className="sign__out d-inline-block d-sm-none" icon={faSignOutAlt} size={'1x'} />
                    </>
                }


            </Col>
        </Row>

        
    </>
}

export default MenuBar;