import Truncate from "react-truncate";
import { Card, CardBody, Row } from "reactstrap";
import { Note } from "../../interfaces/note";
import DeleteNoteButton from "../DeleteNoteButton/DeleteNoteButton";
import EditNoteButton from "../EditNoteButton/EditNoteButton";
import ReactTooltip from 'react-tooltip';
import './NoteDisplay.css'
import { AppConstants } from "../../constants";

const NoteDisplay: React.FunctionComponent<{ note: Note }> = (props) => {

    const { note } = props;

    return <>
        <Card className="note__display" style={{ backgroundColor: note.color, width: `${AppConstants.noteDisplayWidth}px`, height: `${AppConstants.noteDisplayHeight}px` }}>
            <CardBody>
                <DeleteNoteButton note={note}/>
                <Row className="note__title">
                    <h2  style={{width: `${AppConstants.noteTitleWidth}px`}} data-cy="note-display-title" data-tip={note.title} >{note.title}</h2>
                </Row>
                <div data-tip={note.content} className="note__content">
                    <Truncate lines={5} ellipsis={'...'}>
                        {note.content}
                    </Truncate>

                </div>
                <Row className="note__option__container">
                    <div className="note__date"> {new Date(note.created).toLocaleString()} </div>
                    <EditNoteButton {...note}/>
                </Row>

            </CardBody>
        </Card>
        <ReactTooltip  />
    </>
}

export default NoteDisplay;