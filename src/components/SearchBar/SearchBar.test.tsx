import { act, fireEvent, render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import User from "../../interfaces/user";
import { configureStore } from "@reduxjs/toolkit";
import UserReducer from '../../redux/user-slice/user-slice';
import NotesReducer, { NotesState, searchNote } from '../../redux/note-slice/note-slice';
import SearchBar from "./SearchBar";

const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
};

const preloadedStore = (notesState: NotesState) => {
    const store = configureStore({
        reducer: { user: UserReducer, notes: NotesReducer }, preloadedState: {
            notes: notesState,
            user: {
                error: undefined,
                status: 'success',
                user: user
            }
        }
    });

    const origDispatch = store.dispatch;
    store.dispatch = jest.fn(origDispatch) as any;
    return store;
}

describe('Search Bar', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Should dispatch search action with input value', () => {
        const notesState: NotesState = {
            error: undefined,
            notes: undefined,
            searchText: '',
            status: 'idle',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <SearchBar/>
            </Provider>

        );

        const searchInput = screen.getByPlaceholderText('Search');
        const searchText = 'Any search text';

        act(() => {
            fireEvent.change(searchInput, { target: { value: searchText } })
        })

        expect(store.dispatch).toBeCalledWith(searchNote(searchText));
    });

    it('Note State is updated with search text', () => {
        const notesState: NotesState = {
            error: undefined,
            notes: undefined,
            searchText: '',
            status: 'idle',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <SearchBar/>
            </Provider>

        );

        const searchInput = screen.getByPlaceholderText('Search');
        const searchText = 'Any search text';

        act(() => {
            fireEvent.change(searchInput, { target: { value: searchText } })
        })

        expect(store.getState().notes.searchText).toBe(searchText);
    });
})