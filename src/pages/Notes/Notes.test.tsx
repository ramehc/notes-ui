import { act, fireEvent, render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import User from "../../interfaces/user";
import Notes from "./Notes";
import { configureStore } from "@reduxjs/toolkit";
import UserReducer from '../../redux/user-slice/user-slice';
import NotesReducer, { NotesState } from '../../redux/note-slice/note-slice';


const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
};

const preloadedStore = (notesState: NotesState) => {
    const store = configureStore({
        reducer: { user: UserReducer, notes: NotesReducer }, preloadedState: {
            notes: notesState,
            user: {
                error: undefined,
                status: 'success',
                user: user
            }
        }
    });

    store.dispatch = jest.fn();
    return store;
}

describe('Notes Page', () => {

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Should show error screen if notes are not loaded (undefined) and note status is failed', () => {
        const notesState: NotesState = {
            error: 'Error',
            notes: undefined,
            searchText: '',
            status: 'failed',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );

        expect(screen.getByText("Failed to load notes, try again")).toBeInTheDocument();
    });

    it('Should show loading screen if notes are not loaded (undefined) and note status is loading', () => {
        const notesState: NotesState = {
            error: undefined,
            notes: undefined,
            searchText: '',
            status: 'loading',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );

        expect(screen.getByText("Loading notes...")).toBeInTheDocument();
    });

    it('Should show message if no notes has been added', () => {
        const notesState: NotesState = {
            error: undefined,
            notes: [],
            searchText: '',
            status: 'success',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );

        expect(screen.getByText("Click the + on the menu bar to start adding notes")).toBeInTheDocument();
    });

    it('Should show message if search phrase does not match any note', () => {
        const notesState: NotesState = {
            error: undefined,
            notes: [
                {
                    author: 'testUser',
                    color: '#000',
                    content: 'Content',
                    created: new Date().toISOString(),
                    id: 'test',
                    title: 'Title'
                }
            ],
            searchText: 'Any none matching text',
            status: 'success',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );

        expect(screen.getByText("No matches...")).toBeInTheDocument();
    });

    it('Should display loaded notes', () => {
        const note = {
                    author: 'testUser',
                    color: '#000',
                    content: 'Content',
                    created: new Date().toISOString(),
                    id: 'test',
                    title: 'Loaded Notes'
                };
        const notesState: NotesState = {
            error: undefined,
            notes: [
                note
            ],
            searchText: '',
            status: 'success',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );

        expect(screen.getByText(note.title)).toBeInTheDocument();
    });

    it('Should dispatch action when reload button is pressed', () => {
        const notesState: NotesState = {
            error: 'Error',
            notes: undefined,
            searchText: '',
            status: 'failed',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );


        const reloadButton = screen.getByTitle('Reload notes');
        act(() => {
            fireEvent.click(reloadButton);
        });
        
        expect(store.dispatch).toBeCalledTimes(1);
    });

    it('Should dispatch action when note status is idle', () => {
        const notesState: NotesState = {
            error: undefined,
            notes: undefined,
            searchText: '',
            status: 'idle',
            hasMore: true
        };

        const store = preloadedStore(notesState);

        render(
            <Provider store={store}>
                <Notes />
            </Provider>
        );
        
        expect(store.dispatch).toBeCalledTimes(1);
    });

});