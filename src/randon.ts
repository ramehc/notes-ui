import { store } from "./app/store"
import { setUser } from "./redux/user-slice/user-slice"


export const callSomething = () => {
    store.dispatch(setUser({
        firstName: 'Biala',
        lastName: 'Ahmed',
        id: 'id'
    }))
}