import reducer, { deleteNote, editNote, getNotes, NotesState, saveNote, searchNote } from "./note-slice";
import noteAPI from '../../services/api';
import { configureStore } from "@reduxjs/toolkit";
import { Note } from "../../interfaces/note";
import { EditNote } from "../../interfaces/http-request/edit-note";
import { PostNote } from "../../interfaces/http-request/post-note";

const preloadedStore = (initialNotesState: NotesState | undefined = undefined) => {
    const store = configureStore({
        reducer: { notes: reducer }, preloadedState: {
            notes: initialNotesState,
        }
    });

    return store;
}

jest.mock('../../services/api');

describe('Note slice', () => {
    it('should return initial state', () => {
        const store = preloadedStore();

        const state = store.getState().notes;

        expect(state).toEqual({
            notes: undefined,
            searchText: '',
            error: undefined,
            status: 'idle',
            hasMore: true
        });
    });

    it('should handle searching notes', () => {
        const searchString = 'any valid string';
        const store = preloadedStore();
        store.dispatch(searchNote(searchString));

        const state = store.getState().notes;

        expect(state).toEqual(
            {
                notes: undefined,
                searchText: searchString,
                error: undefined,
                status: 'idle',
                hasMore: true
            });
    });

    describe('Get Notes', () => {

        it('should handle fetching notes from API for notes undefined', async () => {
            const store = preloadedStore();
            const notes: Note[] = [
                {
                    author: 'author',
                    color: 'color',
                    content: 'content',
                    created: 'created',
                    id: 'id',
                    title: 'title'
                }
            ];

            (noteAPI.fetchNotes as jest.Mock).mockReturnValueOnce({ notes: notes });

            await store.dispatch(getNotes());
            const state = store.getState().notes;

            expect(state).toEqual({
                notes: notes,
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: true
            });
        });

        it('should set hasMore to false when no notes are returned', async () => {
            const store = preloadedStore();
            const notes: Note[] = [];

            (noteAPI.fetchNotes as jest.Mock).mockReturnValueOnce({ notes: notes });

            await store.dispatch(getNotes());
            const state = store.getState().notes;

            expect(state).toEqual({
                notes: notes,
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: false
            });
        });

        it('should handle fetching notes from API to append exisiting notes list', async () => {

            const previousNotes: Note[] = [
                {
                    author: 'previous author',
                    color: 'previous color',
                    content: 'previous content',
                    created: 'previous created',
                    id: 'previous id',
                    title: '[revious title'
                }
            ];

            const previousState: NotesState = {
                error: undefined,
                notes: previousNotes,
                searchText: '',
                status: 'success',
                hasMore: false
            }

            const store = preloadedStore(previousState);

            const notes: Note[] = [
                {
                    author: 'author',
                    color: 'color',
                    content: 'content',
                    created: 'created',
                    id: 'id',
                    title: 'title'
                }
            ];

            (noteAPI.fetchNotes as jest.Mock).mockReturnValueOnce({ notes: notes });

            await store.dispatch(getNotes());
            const state = store.getState().notes;

            expect(state).toEqual({
                notes: previousNotes.concat(notes),
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: true
            });
        });

        it('should call API.fetchNotes with undefined', async () => {


            const store = preloadedStore();

            await store.dispatch(getNotes());
            expect(noteAPI.fetchNotes).toBeCalledWith(undefined);
        });

        it('should call API.fetchNotes with passed argument', async () => {


            const store = preloadedStore();
            const argument = 'last note id';

            await store.dispatch(getNotes(argument));
            expect(noteAPI.fetchNotes).toBeCalledWith(argument);
        });

        it('should update error message when fetching notes fail', async () => {
            const store = preloadedStore();

            const errorMessage = 'Error fetching notes';

            (noteAPI.fetchNotes as jest.Mock).mockReturnValueOnce(Promise.reject(new Error(errorMessage)));

            await store.dispatch(getNotes());
            const state = store.getState().notes;

            expect(state).toEqual({
                notes: undefined,
                searchText: '',
                error: errorMessage,
                status: 'failed',
                hasMore: true
            });
        });
    });

    describe('Save Note', () => {

        it('should handle adding note when notes are undefined', async () => {
            const store = preloadedStore();
            const note: Note = {
                author: 'author',
                color: 'color',
                content: 'content',
                created: 'created',
                id: 'id',
                title: 'title'
            };

            (noteAPI.postNote as jest.Mock).mockReturnValueOnce(note);

            await store.dispatch(saveNote({
                color: '',
                content: '',
                title: ''
            }));

            const state = store.getState().notes;

            expect(state).toEqual({
                notes: [note],
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: true
            });
        });

        it('should handle adding note to exisiting notes list', async () => {

            const previousNotes: Note[] = [
                {
                    author: 'previous author',
                    color: 'previous color',
                    content: 'previous content',
                    created: 'previous created',
                    id: 'previous id',
                    title: 'previous title'
                }
            ];

            const previousState: NotesState = {
                error: undefined,
                notes: previousNotes,
                searchText: '',
                status: 'success',
                hasMore: true
            }

            const store = preloadedStore(previousState);

            const note: Note =
            {
                author: 'author',
                color: 'color',
                content: 'content',
                created: 'created',
                id: 'id',
                title: 'title'
            };

            (noteAPI.postNote as jest.Mock).mockReturnValueOnce(note);

            await store.dispatch(saveNote(
                {
                    color: '',
                    content: '',
                    title: ''
                }
            ));

            const state = store.getState().notes;

            expect(state).toEqual({
                notes: previousNotes.concat([note]),
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: true
            });
        });

        it('should call API with PostNote object', async () => {


            const store = preloadedStore();
            const add: PostNote =
            {
                color: 'color',
                content: 'content',
                title: 'title'
            };

            await store.dispatch(saveNote(add));
            expect(noteAPI.postNote).toBeCalledWith(add);
        });
    });

    describe('Update Note', () => {

        it('should handle updating note', async () => {

            const note: Note = {
                    author: 'previous author',
                    color: 'previous color',
                    content: 'previous content',
                    created: 'previous created',
                    id: 'previous id',
                    title: 'previous title'
                };

            const previousState: NotesState = {
                error: undefined,
                notes: [note],
                searchText: '',
                status: 'success',
                hasMore: true
            }

            const store = preloadedStore(previousState);

            const editedColor = 'color';
            const editedContent = 'content';
            const editedTitle = 'title'
            const edit: EditNote =
            {
                color: editedColor,
                content: editedContent,
                id: note.id,
                title: editedTitle
            };

            (noteAPI.editNote as jest.Mock).mockReturnValueOnce(true);

            await store.dispatch(editNote(edit));

            const state = store.getState().notes;

            expect(state).toEqual({
                notes: [{
                    ...note,
                    color: editedColor,
                    content: editedContent,
                    title: editedTitle
                }],
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: true
            });
        });

        it('should call API with EditNote object', async () => {


            const store = preloadedStore();
            const edit: EditNote =
            {
                color: 'color',
                content: 'content',
                id: 'id',
                title: 'title'
            };

            await store.dispatch(editNote(edit));
            expect(noteAPI.editNote).toBeCalledWith(edit.id, edit);
        });
    });

    describe('Delete Note', () => {

        it('should handle deleting note', async () => {

            const note: Note = {
                    author: 'previous author',
                    color: 'previous color',
                    content: 'previous content',
                    created: 'previous created',
                    id: 'previous id',
                    title: 'previous title'
                };

            const previousState: NotesState = {
                error: undefined,
                notes: [note],
                searchText: '',
                status: 'success',
                hasMore: true
            }

            const store = preloadedStore(previousState);

            (noteAPI.deleteNote as jest.Mock).mockReturnValueOnce(true);

            await store.dispatch(deleteNote(note.id));

            const state = store.getState().notes;

            expect(state).toEqual({
                notes: [],
                searchText: '',
                error: undefined,
                status: 'success',
                hasMore: true
            });
        });

        it('should call API.deleteNote with note id', async () => {


            const store = preloadedStore();
            const noteId = 'id';

            await store.dispatch(deleteNote(noteId));
            expect(noteAPI.deleteNote).toBeCalledWith(noteId);
        });

    });
});