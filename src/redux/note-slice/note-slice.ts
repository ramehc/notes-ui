import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { EditNote } from '../../interfaces/http-request/edit-note';
import { PostNote } from '../../interfaces/http-request/post-note';
import { Note } from '../../interfaces/note';
import noteAPI from '../../services/api';

export interface NotesState {
    notes: Note[] | undefined;
    searchText: string
    error: string | undefined
    status: 'idle' | 'loading' | 'failed' | 'success';
    hasMore: boolean
}

export const initialNotesState: NotesState = {
    notes: undefined,
    searchText: '',
    error: undefined,
    status: 'idle',
    hasMore: true
};

export const getNotes = createAsyncThunk(
    'notes/getNotes',
    async (lastNoteId: string | undefined) => {
        const response = await noteAPI.fetchNotes(lastNoteId);
        return response.notes;
    }
);

export const saveNote = createAsyncThunk(
    'notes/saveNote',
    async (note: PostNote) => {
        const response = await noteAPI.postNote(note);
        return response;
    }
);

export const editNote = createAsyncThunk(
    'notes/updateNote',
    async (note: EditNote) => {
        await noteAPI.editNote(note.id, note);
        return note;
    }
);

export const deleteNote = createAsyncThunk(
    'notes/deleteNote',
    async (noteId: string) => {
        await noteAPI.deleteNote(noteId);
        return noteId;
    }
);

export const noteSlice = createSlice({
    name: 'notes',
    initialState: initialNotesState,
    reducers: {
        searchNote: (state, action: PayloadAction<string>) => {
            state.searchText = action.payload
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getNotes.rejected, (state, action) => {
                
                if(state.notes === undefined){
                    state.status = "failed";
                }

                state.error = action.error.message;
            })
            .addCase(getNotes.pending, (state) => {
                state.status = "loading";
                state.error = undefined;
            })
            .addCase(getNotes.fulfilled, (state, action) => {
                state.status = "success";
                state.error = undefined;
                state.hasMore = action.payload.length !== 0;
                
                if(!state.notes){
                    state.notes = action.payload;
                    return;
                }

                state.notes = state.notes.concat(action.payload).sort((a, b) => new Date(b.created).getTime() - new Date(a.created).getTime());
            })
            .addCase(saveNote.fulfilled, (state, action) => {
                state.status = "success";
                state.error = undefined

                if(!state.notes){
                    state.notes = [action.payload];
                    return;
                }

                state.notes = state.notes.concat(action.payload).sort((a, b) => new Date(b.created).getTime() - new Date(a.created).getTime());
            })
            .addCase(deleteNote.fulfilled, (state, action) => {
                state.status = "success";
                state.error = undefined

                
                if(state.notes){
                    state.notes = state.notes.filter(note => note.id !== action.payload).sort((a, b) => new Date(b.created).getTime() - new Date(a.created).getTime());
                }
            })
            .addCase(editNote.fulfilled, (state, action) => {
                state.status = "success";
                state.error = undefined

                if(state.notes){
                    const editedNote =  state.notes.find(note => note.id === action.payload.id);
                    if(editedNote){
                        editedNote.content = action.payload.content;
                        editedNote.title = action.payload.title;
                        editedNote.color = action.payload.color;
                    }
                }
            });
    },
});

export const selectNotes = (state: RootState) => state.notes.notes;
export const selectNotesStatus = (state: RootState) => state.notes.status;

export const selectIsEndOfNotes = (state: RootState) => state.notes.hasMore;

export const selectSearchText = (state: RootState) => state.notes.searchText;

export const {searchNote} = noteSlice.actions;

export default noteSlice.reducer;
