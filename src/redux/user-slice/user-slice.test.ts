import { configureStore } from "@reduxjs/toolkit";
import { Auth } from "aws-amplify";
import User from "../../interfaces/user";
import UserService from "../../services/user-service";
import reducer, { loadUser, setUser, signOutUser, UserState } from "../user-slice/user-slice";

const preloadedStore = (initialUserState: UserState | undefined = undefined) => {
    const store = configureStore({
        reducer: { user: reducer }, preloadedState: {
            user: initialUserState,
        }
    });

    return store;
}

const user: User = {
    firstName: 'Test',
    id: 'id',
    lastName: 'User'
};

jest.mock('../../services/user-service');

describe('User Slice', () => {

    it('should return initial state', () => {
        const store = preloadedStore();

        const state = store.getState().user;

        expect(state).toEqual({
            user: undefined,
            error: undefined,
            status: 'idle'
        });
    });

    it('should handle searching notes', () => {
        const store = preloadedStore();
        store.dispatch(setUser(user));

        const state = store.getState().user;

        expect(state).toEqual(
            {
                user,
                error: undefined,
                status: 'idle'
            });
    });

    describe('Sign Out User', () => {

        it('should set user to undefined on successful signout', async () => {
            const store = preloadedStore({
                error: undefined,
                status: 'success',
                user
            });

            await store.dispatch(signOutUser());

            const state = store.getState().user;

            expect(state).toEqual(
                {
                    user: undefined,
                    error: undefined,
                    status: 'idle'
                });
        });

        it('should sign out user of authentication provider', async () => {
            const store = preloadedStore();

            await store.dispatch(signOutUser());

            expect(UserService.signOutUser).toBeCalled();
        });

        it('should handle failure', async () => {

            const errorMessage = 'Error message';

            const store = preloadedStore({
                error: undefined,
                status: 'success',
                user
            });

            (UserService.signOutUser as jest.Mock).mockResolvedValueOnce(Promise.reject(new Error(errorMessage)))

            await store.dispatch(signOutUser());
            const state = store.getState().user;

            expect(state).toEqual(
                {
                    user: user,
                    error: errorMessage,
                    status: 'failed'
                });
        });

    });

    describe('Load User', () => {

        it('should handle load user', async () => {
            const store = preloadedStore();

            (UserService.getUser as jest.Mock).mockResolvedValueOnce(Promise.resolve(user));

            await store.dispatch(loadUser());

            const state = store.getState().user;

            expect(state).toEqual(
                {
                    user: user,
                    error: undefined,
                    status: 'success'
                });
        });

        it('should get user using authentication provider', async () => {
            const store = preloadedStore();

            await store.dispatch(signOutUser());

            expect(UserService.getUser).toBeCalled();
        });

        it('should handle failure', async () => {

            const errorMessage = 'Error message';

            const store = preloadedStore();

            (UserService.getUser as jest.Mock).mockResolvedValueOnce(Promise.reject(new Error(errorMessage)))

            await store.dispatch(loadUser());
            const state = store.getState().user;

            expect(state).toEqual(
                {
                    user: undefined,
                    error: errorMessage,
                    status: 'failed'
                });
        });

    });
});