import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import User from '../../interfaces/user';
import UserService from '../../services/user-service';

export interface UserState {
    user: User | undefined
    error: string | undefined
    status: 'idle' | 'loading' | 'failed' | 'success';
}

export const initialUserState: UserState = {
    user: undefined,
    error: undefined,
    status: 'idle',
};

export const signOutUser = createAsyncThunk(
    'user/signout',
    async () => {
        await UserService.signOutUser();
        return undefined;
    }
);

export const loadUser = createAsyncThunk(
    'user/load',
    async () => {
        return UserService.getUser();
    }
);

export const userSlice = createSlice({
    name: 'user',
    initialState: initialUserState,
    reducers: {
        setUser: (state, action: PayloadAction<User>) => {
            state.user = action.payload
            state.status = 'idle'
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(loadUser.rejected, (state, action) => {
                state.status = "failed";
                state.error = action.error.message;
            })
            .addCase(loadUser.pending, (state) => {
                state.status = "loading";
                state.error = undefined;
            })
            .addCase(loadUser.fulfilled, (state, action) => {
                state.status = "success";
                state.user = action.payload;
                state.error = undefined;
            })
            .addCase(signOutUser.rejected, (state, action) => {
                state.status = "failed";
                state.error = action.error.message;
            })
            .addCase(signOutUser.pending, (state) => {
                state.status = "loading";
                state.error = undefined;
            })
            .addCase(signOutUser.fulfilled, (state) => {
                state.status = "idle";
                state.user = undefined;
                state.error = undefined;
            });
    },
});

export const selectUserState = (state: RootState) => state.user;
export const selectUserStatus = (state: RootState) => state.user.status;
export const { setUser } = userSlice.actions;
export default userSlice.reducer;
