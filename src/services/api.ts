import axios, { AxiosResponse } from "axios";
import { EditNote } from "../interfaces/http-request/edit-note";
import { PostNote } from "../interfaces/http-request/post-note";
import UserService from "./user-service";

enum RequestType {
    get,
    post,
    patch,
    delete
}

const isDefaultSet: () => boolean = () => axios.defaults.baseURL && axios.defaults.headers;


const getNotesBaseUrl = async () => {
    const username = await getUsername();
    const baseUrl = process.env.API_BASE_URL;

    if(!baseUrl){
        throw Error('API_BASE_URL not set in environment variable');
    }

    return `${baseUrl}/users/${username}/notes`;
}

const getUsername = async () => {
    const user = await UserService.getUser();
    if (!user || !user.id) {
        throw new Error('Error getting username');
    }

    return user.id;
}

const setAPIDefaults = async () => {
    axios.defaults.headers = {
        Authorization: await UserService.getAuthToken()
    };
    axios.defaults.baseURL = await getNotesBaseUrl();
}


const request = async (path: string, type: RequestType, body : any | undefined = undefined) => {

    if (!isDefaultSet()) {
        await setAPIDefaults();
    }

    let asyncResponse: Promise<AxiosResponse<any>>;

    switch (type) {
        case RequestType.get:
            asyncResponse = axios.get(path);
            break;
        case RequestType.post:
            asyncResponse = axios.post(path, body);
            break;
        case RequestType.patch:
            asyncResponse = axios.patch(path, body);
            break;
        case RequestType.delete:
            asyncResponse = axios.delete(path);
            break;
    }

    const response = await asyncResponse;

    if (response.status < 200 || response.status >= 300) {
        throw new Error(response.data);
    }

    return response.data;
}

const fetchNotes = async (lastNoteId?: string) => {
    const queryString = lastNoteId ? `?id=${lastNoteId}` : '';
    return request(queryString, RequestType.get);
}

const postNote = async (body: PostNote) => {
    return request('', RequestType.post, body);
}

const deleteNote = async (noteId: string) => {
    return request(`/${noteId}`, RequestType.delete);
}

const editNote = async (noteId: string, body: EditNote) => {
    return request(`/${noteId}`, RequestType.patch, body)
}

const API = {
    fetchNotes,
    postNote,
    deleteNote,
    editNote
}

export default API;