import axios from "axios";
import API from "../api";
import UserService from "../user-service";

const errorMessage = 'Error message';
const username = 'username';
const token = 'token';

jest.mock('axios');

jest.mock('../user-service');
describe('API', () => {

    const OLD_ENV = process.env;

    beforeAll(() => {
        (UserService.getUser as jest.Mock).mockReturnValue(Promise.resolve({ id: username }));
        (UserService.getAuthToken as jest.Mock).mockReturnValue(Promise.resolve(token));
    })

    beforeEach(() => {
        
        process.env = {
            ...OLD_ENV,
            API_BASE_URL: 'baseUrl'
        }
    });

    afterAll(() => {
        process.env = OLD_ENV;
    })

    it('Should throw if response status code is not 2XX', async () => {
        (axios.get as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 404, data: errorMessage }));

        await expect(API.fetchNotes()).rejects.toThrow(errorMessage);
    });

    it('Should throw if user name is undefined', async () => {
        (UserService.getUser as jest.Mock).mockReturnValueOnce(Promise.resolve(undefined));
        axios.defaults.baseURL = undefined;  

        await expect(API.fetchNotes()).rejects.toThrow('Error getting username');
    });

    it('Should throw if API_BASE_URL is not set in environment variable', async () => {
        axios.defaults.baseURL = undefined;  
        delete process.env.API_BASE_URL;

        await expect(API.fetchNotes()).rejects.toThrow('API_BASE_URL not set in environment variable');
    });

    it('Return data if response status code is 2XX', async () => {
        const data = 'any data';
        (axios.get as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 200, data: data }));

        await expect(API.fetchNotes()).resolves.toBe(data);
    });

    describe('fetchNotes', () => {
        it('Calls endpoint with last note id when provided', async () => {
            const lastNoteId = 'id';
            (axios.get as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 200, data: '' }));
            await API.fetchNotes(lastNoteId);
            expect(axios.get).toBeCalledWith(`?id=${lastNoteId}`);
        });

        it('Calls endpoint with empty string when last note id is not provided', async () => {
            (axios.get as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 200, data: '' }));
            await API.fetchNotes();
            expect(axios.get).toBeCalledWith('');
        });
    });


    describe('postNote', () => {
        it('Request body is posted', async () => {
            const body = 'any body';
            (axios.post as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 200, data: '' }));

            await API.postNote(body as any);

            expect(axios.post).toBeCalledWith('', body);
        });
    });

    describe('deleteNote', () => {
        it('Endpoint is called with note id', async () => {
            const noteId = 'noteId';
            (axios.delete as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 200, data: '' }));

            await API.deleteNote(noteId);

            expect(axios.delete).toBeCalledWith(`/${noteId}`);
        });
    });

    describe('editNote', () => {
        it('Endpoint is called with note id and request body', async () => {
            const noteId = 'noteId';
            const body = 'any body';

            (axios.patch as jest.Mock).mockReturnValueOnce(Promise.resolve({ status: 200, data: '' }));

            await API.editNote(noteId, body as any);

            expect(axios.patch).toBeCalledWith(`/${noteId}`, body);
        });
    });

});